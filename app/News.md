# Journal

Annonce des évolutions de l'application et des données qu'elle valorise.

## Nitrates v2

Publiée le 2 décembre 2020

### Evolution des données

- Ajout des données 2019
 
### Evolutions de l'application

- Ajout d'un bouton permettant d'afficher les cartes en plein écran
- Ajout d'un bouton de téléchargement (png, jpeg, pdf et csv) aux diagrammes


## Nitrates v1

Publiée le 6 décembre 2019


